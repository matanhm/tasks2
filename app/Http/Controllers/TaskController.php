<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Hash; 
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;
use App\Task; 
use App\User;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Requests;

class TaskController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $id = Auth::id();
        if (Gate::denies('manager')) {
            $boss = DB::table('employees')->where('employee',$id)->first();
            $id = $boss->manager;
        }
        $user = User::find($id);
        $tasks = $user->tasks;
        return view('tasks.index', compact('tasks'));


        ////////////////////////////////////
        //$id = Auth::id();
        //$id=1;
        //$tasks=Task::all();
        //$tasks=User::find($id)->tasks;
        // view('tasks.index',['tasks'=>$tasks]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (Gate::denies('manager')) {
            abort(403,"Sorry you are not allowed to create a task..");
        } 
        return view('tasks.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $v = Validator::make($request->all(), [
            'task' => 'required|string|max:20',
            ]);
               
            if ($v->fails()) {
                 return redirect()->back()->withErrors($v->errors());
             }

        $task =new Task();
        $id= Auth::id();
        $task->task = $request->task;
        $task->user_id = $id;
        $task->save();
        return redirect('tasks');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if (Gate::denies('manager')) {
            abort(403,"Are you a hacker or what?");
       }
        $task = Task::find($id);
        return view('tasks.edit',compact('task'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $task = Task::findOrFail($id);

        if (Gate::denies('manager')) {
            if ($request->has('task'))
                abort(403,"You are not allowed to edit a task..");
        }   

        if(!$task->user->id == Auth::id()) return(redirect('tasks'));
        $task -> update($request->except(['_token']));
        if($request->ajax()){
            return Response::json(array('result'=>'success', 'status'=>$request->status),200);
        }
        return redirect('tasks');

        //$task = Task::find($id);
      // $task -> update($request->all());
       // return redirect('tasks');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (Gate::denies('manager')) {
            abort(403,"Are you a hacker or what?");
       }
       $task = Task::find($id);
       if(!$task->user->id == Auth::id()) return(redirect('tasks'));
       $task->delete();
       return redirect('tasks');

    }
}
