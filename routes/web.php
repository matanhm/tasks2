<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('/hello/{id?}', function ($id=null) {
    if($id!=null)
    return 'hello student'.$id;
    else
    return 'no student id found';
});
Route::get('/comment/{id?}', function ($id=null) {
    return view('comment',['id'=>$id]);
})->name('comments');

Route::resource('tasks', 'TaskController')->middleware('auth');
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
