<?php

use Illuminate\Database\Seeder;

class TasksTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tasks')->insert(
[
	[
            'task' => 'Buy milk',
            'user_id' => 1,
            'created_at' => date('Y-m-d G:i:s'),
	],
	[
            'task' => 'Prepare for the test',
            'user_id' => 1,
            'created_at' => date('Y-m-d G:i:s'),
	],
	[
            'task' => 'Read a book',
            'user_id' => 1,
            'created_at' => date('Y-m-d G:i:s'),
	],

        ]);
    }

}
