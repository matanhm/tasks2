@extends('layouts.app')
@section('content')

<h1> Edit Your Task </h1>

<form method = 'post' action="{{action('TaskController@update', $task->id)}}">
@csrf
@method('PATCH')
<div class = "form-group">
    <label for = "task">Task to Update:</label>
    <input type= "text" class = "form-control" name= "task" value = "{{$task->task}}">
</div>

<div class = "form-group">
    <input type ="submit" class = "form-control" name="submit" value ="Update Task">
</div>

</form>

<form method = 'post' action="{{action('TaskController@destroy', $task->id)}}">
@csrf
@method('DELETE')
<div class = "form-group">
    <input type ="submit" class = "form-control" name="submit" value ="Delete Task">
</div>

</form>
@endsection