@extends('layouts.app')
@section('content')

<h1> the Bug has been fixed</h1>
<h1> This is The Tasks You Need to Do</h1>
<ul>
    @foreach($tasks as $task)
    <li>
       @if ($task->status)
           <input type = 'checkbox' id ="{{$task->id}}" checked>
       @else
           <input type = 'checkbox' id ="{{$task->id}}">
       @endif
       @cannot('employee') <a href= "{{route('tasks.edit', $task->id )}}">@endcannot {{$task->task}} </a>
    </li>
   
    @endforeach
</ul>


@cannot('employee')<a href= "{{route('tasks.create')}}"> @endcannot Create a New Task </a>
<script>
       $(document).ready(function(){
           $(":checkbox").click(function(event){
               $.ajax({
                   url: "{{url('tasks')}}" + '/' + event.target.id ,
                   dataType:'json' ,
                   type:'put',
                   contentType:'application/json',
                   data: JSON.stringify({'status':event.target.checked, _token:'{{csrf_token()}}'}),
                   processData:false,
                   success: function( data){
                        console.log(JSON.stringify( data ));
                   },
                   error: function(errorThrown ){
                       console.log( errorThrown );
                   }
               });   
              });
       });
</script>  
            

@endsection