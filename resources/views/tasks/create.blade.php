@extends('layouts.app')
@section('content')

<h1>Create a New Task</h1>

<form method = 'post' action="{{action('TaskController@store')}}">
{{csrf_field()}}

<div class = "form-group">
    <label for = "task">What Task Would You Like to Add?</label>
    <input type= "text" class = "form-control" name= "task">
</div>

@foreach($errors->all() as $error)
   <p>{{$error}}</p>
@endforeach

<div class = "form-group">
    <input type ="submit" class = "form-control" name="submit" value ="Add Task">
</div>

</form>
@endsection